%define debug_package %{nil}
%bcond_with debug

%global platform_commit 728c8bb974be69b4034fad7a1c60917cca2dd03d
%global edk2_commit fbe0805b2091393406952e84724188f8c1941837
%global shortcommit %(c=%{edk2_commit}; echo ${c:0:7})
%global shortcommit_platform %(c=%{platform_commit}; echo ${c:0:7})
%global commit_date 20250221

# Build options
%global TOOL_CHAIN_TAG GCC5
%global TARGET_ARCH AARCH64
%global ACTIVE_PLATFORM Platform/RaspberryPi/RPi4/RPi4.dsc
%global DEFAULT_KEYS -D DEFAULT_KEYS=TRUE -D PK_DEFAULT_FILE=$WORKSPACE/keys/pk.cer -D KEK_DEFAULT_FILE1=$WORKSPACE/keys/ms_kek1.cer -D KEK_DEFAULT_FILE1=$WORKSPACE/keys/ms_kek2.cer -D DB_DEFAULT_FILE1=$WORKSPACE/keys/ms_db1.cer -D DB_DEFAULT_FILE2=$WORKSPACE/keys/ms_db2.cer -D DB_DEFAULT_FILE3=$WORKSPACE/keys/ms_db3.cer -D DB_DEFAULT_FILE4=$WORKSPACE/keys/ms_db4.cer -D DBX_DEFAULT_FILE1=$WORKSPACE/keys/arm64_dbx.bin
%global BUILD_FLAGS -D SECURE_BOOT_ENABLE=TRUE -D INCLUDE_TFTP_COMMAND=TRUE -D NETWORK_ISCSI_ENABLE=TRUE -D SMC_PCI_SUPPORT=1
%if %{with debug}
%global TARGET DEBUG
%else
%global TARGET RELEASE
%endif

Name:           edk2-rpi4
Version:        %{commit_date}git%{shortcommit}platform%{shortcommit_platform}

%global WORKSPACE %{_builddir}/%{name}-%{version}

Release:        1%{?dist}
Summary:        UEFI firmware for Raspberry Pi 4
License:        BSD-2-Clause-Patent
URL:            http://www.tianocore.org
Source0:        config.txt

ExclusiveArch:	aarch64

# Enable more than 3GB and ACPI+Devicetree by default
Patch0:         edk2-rpi4-defaults.patch


BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  make
BuildRequires:  python3
BuildRequires:  git
BuildRequires:  libuuid-devel
BuildRequires:  acpica-tools
BuildRequires:  openssl
BuildRequires:  efi-filesystem
BuildRequires:  efi-srpm-macros
BuildRequires:  arm-trusted-firmware-armv8

Requires:       bcm2711-firmware

%description
This is a port of 64-bit Tiano Core UEFI firmware for the Raspberry Pi 4 platform.

This is intended to be useful 64-bit TF-A + UEFI implementation for the Raspberry Pi variants based on the BCM2711 SoC, which should be good enough for most kind of UEFI development, as well running consumer Operating Systems.

Raspberry Pi is a trademark of the Raspberry Pi Foundation.

%prep
mkdir -p %{WORKSPACE}

%build
pushd %{WORKSPACE}
    export WORKSPACE=%{WORKSPACE}
    git clone https://github.com/tianocore/edk2.git
    pushd edk2
        git checkout %{edk2_commit}
        git submodule update --init
    popd

    git clone https://github.com/tianocore/edk2-platforms.git
    pushd edk2-platforms
        git checkout %{platform_commit}
        git submodule update --init
        %{__patch} -p1 < %{PATCH0}
    popd

    git clone https://github.com/tianocore/edk2-non-osi.git
    # Copy bl31
    install %{_datadir}/arm-trusted-firmware/rpi4/bl31.bin edk2-non-osi/Platform/RaspberryPi/RPi4/TrustedFirmware/bl31.bin

    export PACKAGES_PATH=$PWD/edk2:$PWD/edk2-platforms:$PWD/edk2-non-osi

    . edk2/edksetup.sh

    %{__make} -C edk2/BaseTools

    # Now before building set up Secure Boot default keys
    # From https://github.com/pftf/RPi4/blob/master/.github/workflows/linux_edk2.yml
    mkdir keys
    openssl req -new -x509 -newkey rsa:2048 -subj "/CN=Raspberry Pi 4 Platform Key/" -keyout /dev/null -outform DER -out keys/pk.cer -days 7300 -nodes -sha256
    curl -L https://go.microsoft.com/fwlink/?LinkId=321185 -o keys/ms_kek1.cer
    curl -L https://go.microsoft.com/fwlink/?linkid=2239775 -o keys/ms_kek2.cer
    curl -L https://go.microsoft.com/fwlink/?linkid=321192 -o keys/ms_db1.cer
    curl -L https://go.microsoft.com/fwlink/?linkid=321194 -o keys/ms_db2.cer
    curl -L https://go.microsoft.com/fwlink/?linkid=2239776 -o keys/ms_db3.cer
    curl -L https://go.microsoft.com/fwlink/?linkid=2239872 -o keys/ms_db4.cer
    curl -L https://uefi.org/sites/default/files/resources/dbxupdate_arm64.bin -o keys/arm64_dbx.bin

    build -n $(nproc) \
        -b %{TARGET} \
        -a %{TARGET_ARCH} \
        -t %{TOOL_CHAIN_TAG} \
        -p %{ACTIVE_PLATFORM} \
        --pcd gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVendor=L"EDK II for Raspberry Pi 4" \
        --pcd gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVersionString=L"UEFI for RPI4 %{commit_date}" \
        %{BUILD_FLAGS} %{DEFAULT_KEYS}
popd

%install
pushd %{WORKSPACE}
    mkdir -p %{buildroot}%{efi_esp_root}
    install -m 0600 Build/RPi4/%{TARGET}_GCC5/FV/RPI_EFI.fd %{buildroot}%{efi_esp_root}/
    install -m 0600 %{SOURCE0} %{buildroot}%{efi_esp_root}/
    # Text files
    install -m 0644 edk2-platforms/License.txt %{_builddir}
    install -m 0644 edk2-platforms/Readme.md %{_builddir}
    install -m 0644 edk2-platforms/License-History.txt %{_builddir}
popd

%files
%config(noreplace) %{efi_esp_root}/config.txt
%license License.txt
%doc Readme.md License-History.txt
%{efi_esp_root}/RPI_EFI.fd

%changelog
* Tue Feb 25 2025 Luca Magrone <luca@magrone.cc> 20250221.gitfbe0805platform728c8bb-1
- Update edk2 to fbe0805
- Update platform to 728c8bb

* Tue Sep 24 2024 Luca Magrone <luca@magrone.cc> 20240813.gitb158dadplatform6146fd7-1
- Update edk2 to stable tag 202408
- Update platform to 6146fd7
- Add the 2023 Microsoft Secure Boot certificates

* Sun May 05 2024 Luca Magrone <luca@magrone.cc> 20240410.git963671dplatform45f9683-1
- Update edk2 to 963671d
- Update platform to 45f9683

* Mon Oct 09 2023 Luca Magrone <luca@magrone.cc> 20230603.gitc1dd400platform040ecd8-1
- Require bcm2711-firmware instead of the whole bcm283x-firmware
- Update edk2 to 20230603
- Update platform to 040ecd8

* Mon Nov 28 2022 Luca Magrone <luca@magrone.cc> 20220829gitba0e0e4platform3c3b116-1
- Update edk2 to 20220829
- Update platform to 3c3b116
- Drop patch from https://github.com/pftf/RPi4

* Sun Nov 27 2022 Luca Magrone <luca@magrone.cc> 20220225gitb24306fplatformb0f3f90-1
- Use edk2 repo versioning and add platform commit in the version
- Update edk2 to 20220225

* Sun Nov 27 2022 Luca Magrone <luca@magrone.cc> 20220303git958fc02-1
- Change snapshot versioning
- Use TF-A from arm-trusted-firmware

* Sat Nov 26 2022 Luca Magrone <luca@magrone.cc> 0-13.20220303.git958fc02
- Add configuration entries to config.txt
- Use config.txt as only source file

* Thu Nov 24 2022 Luca Magrone <luca@magrone.cc> 0-12.20220303.git958fc02
- Use same revisions as https://github.com/pftf/RPi4

* Thu Nov 24 2022 Luca Magrone <luca@magrone.cc> 0-11.20220208.gitb0f3f90
- Use commit b0f3f90

* Thu Nov 24 2022 Luca Magrone <luca@magrone.cc> 0-10.20221010.gitad00518
- Build non-debug version by default
- Use lastest relevant commit (ad00518)

* Sat Oct 29 2022 Luca Magrone <luca@magrone.cc> - 0-9.20221019.git0286233
- Drop requirement for rpi4-firmware
- Require bcm283x-firmware
- Use macro for efi path
- Remove symlink

* Sun Oct 23 2022 Luca Magrone <luca@magrone.cc> - 0-8.20221019.git0286233
- Update to commit 0286233
- Sign the binary

* Sun Oct 16 2022 Luca Magrone <luca@magrone.cc> - 0-7.20221012.git4d99e03
- Shorten version strings

* Sat Oct 15 2022 Luca Magrone <luca@magrone.cc> - 0-6.20221012.git4d99e03
- Add versioning to firmware
- Fix changelog
- Update to commit 4d99e03
- Add link to the binary file

* Fri Oct 14 2022 Luca Magrone <luca@magrone.cc> - 0-3.20221010.gitad00518
- Update to include fix xhci in dt mode
- Switch to git relative versioning

* Sat Aug 20 2022 Luca Magrone <luca.magrone@me.com> - 0-2
- Update to include new library instance from SecurityPkg

* Fri Jul 1 2022 Luca Magrone <luca.magrone@me.com> - 0-1
- Initial version of the package
